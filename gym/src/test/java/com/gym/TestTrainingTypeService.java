package com.gym;

import com.gym.configuration.GymConfiguration;
import com.gym.entity.TrainingTypeEntity;
import com.gym.service.TrainingTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
@RunWith(SpringRunner.class)
@SpringJUnitConfig(GymConfiguration.class)
public class TestTrainingTypeService {

    @Autowired
    TrainingTypeService trainingTypeService;

    @Test
    public void testGetById(){

        TrainingTypeEntity trainingType = trainingTypeService.findById(1L);
        assertEquals(trainingType.getTrainingTypeName(), "Jogging");

        trainingType = trainingTypeService.findById(2L);
        assertEquals(trainingType.getTrainingTypeName(), "Wrestling");

        trainingType = trainingTypeService.findById(3L);
        assertEquals(trainingType.getTrainingTypeName(), "Something");

        trainingType = trainingTypeService.findById(4L);
        assertEquals(trainingType.getTrainingTypeName(), "Swimming");

        trainingType = trainingTypeService.findById(5L);
        assertEquals(trainingType.getTrainingTypeName(), "Bulking");

        trainingType = trainingTypeService.findById(6L);
        assertEquals(trainingType.getTrainingTypeName(), "Going insane");

        trainingType = trainingTypeService.findById(7L);
        assertEquals(trainingType.getTrainingTypeName(), "Gachi muching");
    }


}
