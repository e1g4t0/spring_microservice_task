package com.gym;

import com.gym.configuration.GymConfiguration;
import com.gym.entity.TraineeEntity;
import com.gym.entity.UserEntity;
import com.gym.service.TraineeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@RunWith(SpringRunner.class)
@SpringJUnitConfig(GymConfiguration.class)
public class TraineeServiceTest {

    @Autowired
    TraineeService traineeService;
    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

    @Test
    public void getTraineeByUsername() throws ParseException {

        TraineeEntity trainee = traineeService.create("NewUser", "Surname", dateFormat.parse("12-12-12"), "qwerty");
        TraineeEntity traineeByUsername = traineeService.getTraineeByUserName(trainee.getUser().getUsername());

        assertEquals(trainee, traineeByUsername);
        traineeService.delete(trainee.getUser().getUsername());
    }
    @Test
    public void testCreateTrainee() throws ParseException {

        TraineeEntity trainee = traineeService.create("NewUser", "Surname", dateFormat.parse("12-12-12"), "qwerty");
        assertNotNull(trainee);
        traineeService.delete(trainee.getUser().getUsername());
    }

    @Test
    public void testDeleteTrainee() throws ParseException {

        TraineeEntity trainee = traineeService.create("NewUser", "Surname", dateFormat.parse("12-12-12"), "qwerty");
        assertNotNull(trainee);
        String traineeUsername = trainee.getUser().getUsername();
        traineeService.delete(traineeUsername);

        trainee = traineeService.getTraineeByUserName(traineeUsername);
        assertNull(trainee);
    }

    @Test
    public void testUpdateTrainee() throws ParseException {

        TraineeEntity trainee = traineeService.create("NewUser", "Surname", dateFormat.parse("12-12-12"), "qwerty");

        trainee = traineeService.updateTraineeProfile(trainee.getUser().getUsername(), "Credentials", "Updated", "NewAddress", null, true);

        UserEntity user = trainee.getUser();
        assertEquals("Credentials", user.getFirstName());
        assertEquals("Updated", user.getLastName());
        assertEquals("NewAddress", trainee.getAddress());

        traineeService.delete(user.getUsername());
    }

    @Test
    public void testGenerateUsername(){

        String username = traineeService.generateUsername("Unique", "Username");
        assertEquals("Unique.Username", username);

        username = traineeService.generateUsername("John", "Doee");
        assertEquals("John.Doee", username);

    }
    @Test
    public void testChangeActivationStatus() throws ParseException {
        TraineeEntity trainee = traineeService.create("NewUser", "Surname", dateFormat.parse("12-12-12"), "qwerty");
        UserEntity user = trainee.getUser();

        assertTrue(user.isActive());

        trainee = traineeService.changeActivationStatus(user.getUsername());
        user = trainee.getUser();
        assertFalse(user.isActive());

        trainee = traineeService.changeActivationStatus(user.getUsername());
        user = trainee.getUser();
        assertTrue(user.isActive());

        traineeService.delete(user.getUsername());
    }
}
