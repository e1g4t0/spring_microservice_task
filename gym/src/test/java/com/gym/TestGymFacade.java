package com.gym;

import com.gym.configuration.GymConfiguration;
import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.facade.GymFacade;
import com.gym.repository.TraineeRepository;
import com.gym.repository.TrainerRepository;
import com.gym.repository.TrainingRepository;
import com.gym.repository.TrainingTypeRepository;
import com.gym.service.TraineeService;
import com.gym.service.TrainerService;
import com.gym.service.TrainingService;
import com.gym.service.TrainingTypeService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.SecondaryTable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
@SpringJUnitConfig(GymConfiguration.class)
public class TestGymFacade {

    @Autowired
    GymFacade gymFacade;
    @Autowired
    TraineeRepository traineeRepository;
    @Autowired
    TrainerRepository trainerRepository;
    @Autowired
    TrainingRepository trainingRepository;
    @Autowired
    TrainingTypeRepository trainingTypeRepository;
    @Autowired
    TraineeService traineeService;
    @Autowired
    TrainerService trainerService;
    @Autowired
    TrainingService trainingService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");



    @Test
    public void getAllTraineeTrainingsTest() throws ParseException {

        TrainerEntity trainer = trainerService.create("UniqueTrainerName", "Surname", "Awesome specialization");
        TraineeEntity trainee = traineeService.create("NewTraineeNamee", "Surname",dateFormat.parse("2005-12-15"), "qwerty");

        TrainingTypeEntity trainingType = trainingTypeRepository.findByTrainingTypeName("Jogging");

        TrainingEntity trainingEntity1 = gymFacade.addTraining(trainee.getUser().getUsername(), trainer.getUser().getUsername(), "Jogging session", trainingType.getId() ,dateFormat.parse("2012-12-15"), 2.0);
        TrainingEntity trainingEntity = trainingService.create(trainee, trainer, "Jogging session", trainingType, dateFormat.parse("2012-12-13"), 2.0);

        List<TrainingEntity> trainingEntities = gymFacade.getAllTraineeTrainings(trainee.getUser().getUsername(), trainer.getUser().getUsername(),
                "Jogging session", dateFormat.parse("2012-12-12"), dateFormat.parse("2012-12-15"), trainingType);


        Set<TrainingEntity> trainingEntitySet = new HashSet<>();

        trainingEntitySet.add(trainingEntity);
        trainingEntitySet.add(trainingEntity1);

        Assertions.assertEquals(trainingEntitySet.size(), trainingEntities.size());
        Assertions.assertTrue(trainingEntitySet.containsAll(trainingEntities));
        System.out.println(trainee);
        System.out.println(trainer);

        trainingRepository.delete(trainingEntity);
        trainingRepository.delete(trainingEntity1);
        traineeRepository.delete(trainee);
        trainerRepository.delete(trainer);


    }
}
