package com.gym;

import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.repository.TraineeRepository;
import com.gym.repository.TrainerRepository;
import com.gym.repository.TrainingRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc()
public class TrainingControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TrainingRepository trainingRepository;
    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private TraineeRepository traineeRepository;

    @Test
    public void testCreateTraining() throws Exception {
        MvcResult resultTrainee = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=testGet&lastname=mockUser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        String contentTrainee = resultTrainee.getResponse().getContentAsString();
        String usernameTrainee = contentTrainee.substring(contentTrainee.indexOf(":") + 2, contentTrainee.indexOf("password") - 3);
        String passwordTrainee = contentTrainee.substring(contentTrainee.lastIndexOf(":") + 2, contentTrainee.lastIndexOf(":") + 12);


        MvcResult resultTrainer = mockMvc.perform(MockMvcRequestBuilders.post("/trainers?firstname=Ricardo&lastname=Milos&specialization=Jogging"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        String contentTrainer = resultTrainer.getResponse().getContentAsString();
        String usernameTrainer = contentTrainer.substring(contentTrainer.indexOf(":") + 2, contentTrainer.indexOf("password") - 3);
        String passwordTrainer = contentTrainer.substring(contentTrainer.lastIndexOf(":") + 2, contentTrainer.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainers/login?username=" + usernameTrainer +"&password=" + passwordTrainer))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String token = resultLogin.getResponse().getContentAsString();

        String trainingURI = "/trainings?traineeUsername=" + usernameTrainee + "&trainerUsername=" + usernameTrainer +"&trainingName=Swimming Session&dateOfTraining=2022-07-15&duration=2.0&trainingType=Jogging";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(trainingURI).header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        //TrainingEntity training = trainingRepository.

    }
}
