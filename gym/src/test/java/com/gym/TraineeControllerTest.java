package com.gym;

import com.gym.entity.TraineeEntity;
import com.gym.repository.TraineeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc()
public class TraineeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TraineeRepository traineeRepository;

    @Test
    public void testCreateTrainee() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=test&lastname=mockuser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);

        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        traineeRepository.delete(trainee);
    }

    @Test
    public void testLoginTrainee() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=test&lastname=mockuser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        traineeRepository.delete(trainee);
    }

    @Test
    public void testGetTrainee() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=testGet&lastname=mockUser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String token = resultLogin.getResponse().getContentAsString();

        MvcResult getResult = mockMvc.perform(MockMvcRequestBuilders.get("/trainees").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        System.out.println(getResult);

        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        traineeRepository.delete(trainee);
    }

    @Test
    public void testTraineeChangePassword() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=testGet&lastname=mockUser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String token = resultLogin.getResponse().getContentAsString();

        MvcResult changeLogin = mockMvc.perform(MockMvcRequestBuilders.put("/trainees/login?newPassword=" + "NewPassword").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        traineeRepository.delete(trainee);
    }

    @Test
    public void testLoginTraineeIncorrectCredentials() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=test&lastname=mockuser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + "INCORRECTPASSWORD"))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andReturn();

        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        traineeRepository.delete(trainee);
    }


    @Test
    public void testLoginTraineeIncorrectCredentialsBruteForce() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=test&lastname=mockuser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);


        for (int i = 0; i < 4; i++) {
            MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + "INCORRECTPASSWORD"))
                    .andExpect(MockMvcResultMatchers.status().isForbidden())
                    .andReturn();
        }

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andReturn();


        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        traineeRepository.delete(trainee);
    }

    @Test
    public void testDeleteTrainee() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainees?firstname=testGet&lastname=mockUser&dateOfBirth=2000-12-11&address=trainee address"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainees/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String token = resultLogin.getResponse().getContentAsString();

        MvcResult getResult = mockMvc.perform(MockMvcRequestBuilders.delete("/trainees").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
}
