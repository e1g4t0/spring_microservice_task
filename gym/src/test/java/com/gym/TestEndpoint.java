package com.gym;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/test")
public class TestEndpoint {

    @GetMapping
    ResponseEntity<String> helloWorld(){

        return new ResponseEntity<>("Hello World!", HttpStatus.OK);
    }
}
