package com.gym;

import com.gym.configuration.GymConfiguration;
import com.gym.dto.*;
import com.gym.entity.*;
import com.gym.mapper.TraineeMapper;
import com.gym.mapper.TrainerMapper;
import com.gym.mapper.TrainingMapper;
import com.gym.mapper.UserCredentialsMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Date;


public class MappersTest {

    @Test
    public void setUserCredentialsMapperTest(){
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName("name");
        userEntity.setLastName("surname");

        UserCredentialsDTO userCredentialsDTO = UserCredentialsMapper.getUserCredentialsDTOFromUser(userEntity);

        Assertions.assertEquals(userCredentialsDTO.getUsername(), userEntity.getUsername());
        Assertions.assertEquals(userCredentialsDTO.getPassword(), userEntity.getPassword());
    }

    @Test
    public void TraineeForTrainerMapperTest(){
        UserEntity userEntity = new UserEntity();

        userEntity.setFirstName("name");
        userEntity.setLastName("surname");
        userEntity.setUsername("name.surname");
        TraineeEntity trainee = new TraineeEntity();
        trainee.setUser(userEntity);
        TraineeForTrainerDTO traineeForTrainerDTO = TraineeMapper.getTraineeForTrainerDTO(trainee);

        Assertions.assertEquals(traineeForTrainerDTO.getLastName(), userEntity.getLastName());
        Assertions.assertEquals(traineeForTrainerDTO.getFirstName(), userEntity.getFirstName());
        Assertions.assertEquals(traineeForTrainerDTO.getUsername(), userEntity.getUsername());
    }

    @Test
    public void setTraineeMapperTest(){

        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName("name");
        userEntity.setLastName("surname");
        userEntity.setActive(true);
        TraineeEntity traineeEntity = new TraineeEntity();
        traineeEntity.setUser(userEntity);
        traineeEntity.setAddress("asdff");
        TraineeDTO traineeDTO = TraineeMapper.getTraineeDTOFromTraineeEntity(traineeEntity);

        Assertions.assertEquals(traineeDTO.getUsername(), userEntity.getUsername());
        Assertions.assertEquals(traineeDTO.getFirstName(), userEntity.getFirstName());
        Assertions.assertEquals(traineeDTO.getLastName(), userEntity.getLastName());
        Assertions.assertEquals(traineeDTO.getAddress(), traineeEntity.getAddress());
        Assertions.assertEquals(traineeDTO.isActive(), userEntity.isActive());

    }

    @Test
    public void setTrainerMapper(){
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName("name");
        userEntity.setLastName("surname");
        userEntity.setActive(true);

        TrainerEntity trainer = new TrainerEntity();
        trainer.setUser(userEntity);
        trainer.setSpecialization("Jogging");

        TrainerDTO trainerDTO = TrainerMapper.getTrainerDTOFromTrainerEntity(trainer);


        Assertions.assertEquals(trainerDTO.getUsername(), userEntity.getUsername());
        Assertions.assertEquals(trainerDTO.getFirstName(), userEntity.getFirstName());
        Assertions.assertEquals(trainerDTO.getLastName(), userEntity.getLastName());
        Assertions.assertEquals(trainerDTO.getSpecialization(), trainer.getSpecialization());
        Assertions.assertEquals(trainerDTO.getActive(), userEntity.isActive());

    }

    @Test
    public void  testTrainerDTOForTraineeEntity(){

        UserEntity userEntity = new UserEntity();

        userEntity.setFirstName("name");
        userEntity.setLastName("surname");
        userEntity.setUsername("name.surname");

        TrainerEntity trainer = new TrainerEntity();
        trainer.setUser(userEntity);
        trainer.setSpecialization("Jogging");
        TrainerForTraineeDTO trainerForTraineeDTO = TrainerMapper.getTrainerForTraineeDTOFromTrainerEntity(trainer);

        Assertions.assertEquals(trainerForTraineeDTO.getLastName(), userEntity.getLastName());
        Assertions.assertEquals(trainerForTraineeDTO.getFirstName(), userEntity.getFirstName());
        Assertions.assertEquals(trainerForTraineeDTO.getUsername(), userEntity.getUsername());
        Assertions.assertEquals(trainerForTraineeDTO.getSpecialization(), trainer.getSpecialization());


    }

    @Test
    public void testTrainingDTO(){

        TrainingEntity trainingEntity = new TrainingEntity();
        trainingEntity.setTrainingName("Cool training");
        trainingEntity.setTrainingDuration(2.0);

        TrainingTypeEntity trainingTypeEntity = new TrainingTypeEntity();
        trainingTypeEntity.setId(1L);
        trainingTypeEntity.setTrainingTypeName("Jogging");
        trainingEntity.setTrainingType(trainingTypeEntity);
        trainingEntity.setTrainingDate(new Date());

        UserEntity userEntityTrainee = new UserEntity();
        userEntityTrainee.setFirstName("name");
        userEntityTrainee.setLastName("surname");
        userEntityTrainee.setUsername("name.surname");
        TraineeEntity trainee = new TraineeEntity();
        trainee.setUser(userEntityTrainee);

        UserEntity userEntityTrainer = new UserEntity();
        userEntityTrainee.setFirstName("newName");
        userEntityTrainee.setLastName("newSurname");
        userEntityTrainee.setUsername("newName.newSurname");
        TrainerEntity trainer = new TrainerEntity();
        trainer.setUser(userEntityTrainer);


        trainingEntity.setTrainee(trainee);
        trainingEntity.setTrainer(trainer);
        TrainingDTO trainingDTO = TrainingMapper.getTrainingDTOFromTrainingEntity(trainingEntity);

        Assertions.assertEquals(trainingEntity.getTrainingName(), trainingDTO.getTrainingName());
        Assertions.assertEquals(trainingEntity.getTrainingType(), trainingDTO.getTrainingType());
        Assertions.assertEquals(trainingEntity.getTrainingDate(), trainingDTO.getTrainingDate());
        Assertions.assertEquals(trainingEntity.getTrainee().getUser().getUsername(), trainingDTO.getTraineeUsername());
        Assertions.assertEquals(trainingEntity.getTrainer().getUser().getUsername(), trainingDTO.getTrainerUsername());
        Assertions.assertEquals(trainingEntity.getTrainingDuration(), trainingDTO.getTrainingDuration());

    }

}
