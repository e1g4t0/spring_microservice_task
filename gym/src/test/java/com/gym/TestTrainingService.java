package com.gym;

import com.gym.configuration.GymConfiguration;
import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.service.TraineeService;
import com.gym.service.TrainerService;
import com.gym.service.TrainingService;
import com.gym.service.TrainingTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertNotNull;
@SpringBootTest
@RunWith(SpringRunner.class)
@SpringJUnitConfig(GymConfiguration.class)
public class TestTrainingService {

    @Autowired
    TraineeService traineeService;
    @Autowired
    TrainerService trainerService;
    @Autowired
    TrainingService trainingService;
    @Autowired
    TrainingTypeService trainingTypeService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
    @Test
    public void testCreateTraining() throws ParseException {


        TraineeEntity trainee = traineeService.create("NewTrainee", "Surname", dateFormat.parse("2012-12-12"), "trainee address");
        TrainerEntity trainer = trainerService.create("NewTrainer", "Surname", "Awesome specialization");
        TrainingTypeEntity trainingType = trainingTypeService.findById(1L);

        TrainingEntity retrievedTraining = trainingService.create(trainee, trainer, "Just a session", trainingType,
                dateFormat.parse("2012-12-12"), 2.0);

        assertNotNull(retrievedTraining);

        traineeService.delete(trainee.getUser().getUsername());
        trainerService.delete(trainer.getUser().getUsername());
    }
}
