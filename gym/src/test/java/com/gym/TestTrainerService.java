package com.gym;

import com.gym.configuration.GymConfiguration;
import com.gym.entity.TrainerEntity;
import com.gym.entity.UserEntity;
import com.gym.service.TrainerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@RunWith(SpringRunner.class)
@SpringJUnitConfig(GymConfiguration.class)
public class TestTrainerService {

    @Autowired
    TrainerService trainerService;

    @Test
    public void getTrainerByUsername(){

        TrainerEntity trainer = trainerService.create("NewTrainer", "Surname", "Awesome specialization");
        TrainerEntity trainerByUsername = trainerService.getTrainerByUserName(trainer.getUser().getUsername());

        assertEquals(trainer, trainerByUsername);
        trainerService.delete(trainerByUsername.getUser().getUsername());
    }
    @Test
    public void testCreateTrainer(){

        TrainerEntity trainer = trainerService.create("NewTrainer", "Surname", "Awesome specialization");
        assertNotNull(trainer);
        trainerService.delete(trainer.getUser().getUsername());
    }

    @Test
    public void testDeleteTrainer(){

        TrainerEntity trainer = trainerService.create("NewTrainer", "Surname", "Awesome specialization");
        assertNotNull(trainer);
        String trainerUsername = trainer.getUser().getUsername();
        trainerService.delete(trainerUsername);

        trainer = trainerService.getTrainerByUserName(trainerUsername);
        assertNull(trainer);
    }

    @Test
    public void testUpdateTrainer(){

        TrainerEntity trainer = trainerService.create("NewTrainer", "Surname", "Awesome specialization");

        trainer = trainerService.updateTrainerProfile(trainer.getUser().getUsername(), "Updated", "Trainer", "Even better specialization", true);

        UserEntity user = trainer.getUser();
        assertEquals("Updated", user.getFirstName());
        assertEquals("Trainer", user.getLastName());
        assertEquals("Even better specialization", trainer.getSpecialization());

        trainerService.delete(user.getUsername());
    }
    @Test
    public void testGenerateUsername(){

        String username = trainerService.generateUsername("Best", "UserName");
        assertEquals("Best.UserName", username);

        username = trainerService.generateUsername("Best", "Trainer");
        assertEquals("Best.Trainer", username);

    }
    @Test
    public void testChangeActivationStatus(){
        TrainerEntity trainer = trainerService.create("NewTrainer", "Surname", "Awesome specialization");
        UserEntity user = trainer.getUser();

        assertTrue(user.isActive());

        trainer = trainerService.changeActivationStatus(user.getUsername());
        user = trainer.getUser();
        assertFalse(user.isActive());

        trainer = trainerService.changeActivationStatus(user.getUsername());
        user = trainer.getUser();
        assertTrue(user.isActive());

        trainerService.delete(user.getUsername());
    }
}
