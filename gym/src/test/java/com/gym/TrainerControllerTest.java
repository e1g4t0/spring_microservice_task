package com.gym;

import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.repository.TraineeRepository;
import com.gym.repository.TrainerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc()
public class TrainerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TrainerRepository trainerRepository;

    @Test
    public void testCreateTrainer() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainers?firstname=Ricardo&lastname=Milos&specialization=Jogging"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);

        TrainerEntity trainer = trainerRepository.findByUserUsername(username);
        trainerRepository.delete(trainer);
    }

    @Test
    public void testLoginTrainer() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainers?firstname=drop&lastname=trainer&specialization=Jogging"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainers/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        TrainerEntity trainer = trainerRepository.findByUserUsername(username);
        trainerRepository.delete(trainer);
    }

    @Test
    public void testGetTrainer() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainers?firstname=drop&lastname=trainer&specialization=Jogging"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainers/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String token = resultLogin.getResponse().getContentAsString();

        MvcResult getResult = mockMvc.perform(MockMvcRequestBuilders.get("/trainers").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        System.out.println(getResult);

        TrainerEntity trainer = trainerRepository.findByUserUsername(username);
        trainerRepository.delete(trainer);
    }

    @Test
    public void testTrainerChangePassword() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainers?firstname=drop&lastname=trainer&specialization=Jogging"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.lastIndexOf(":") + 2, content.lastIndexOf(":") + 12);

        MvcResult resultLogin = mockMvc.perform(MockMvcRequestBuilders.get("/trainers/login?username=" + username +"&password=" + password))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String token = resultLogin.getResponse().getContentAsString();

        MvcResult changeLogin = mockMvc.perform(MockMvcRequestBuilders.put("/trainers/login?newPassword=" + "NewPassword").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        TrainerEntity trainer = trainerRepository.findByUserUsername(username);
        trainerRepository.delete(trainer);
    }
}
