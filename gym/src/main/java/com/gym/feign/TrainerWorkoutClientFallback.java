package com.gym.feign;

import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class TrainerWorkoutClientFallback implements TrainerWorkloadClient{
    @Override
    public String addTraining(String token, String username, String firstname, String lastname, Boolean isActive, Date date, Double duration) {
        return "fallback";
    }
}
