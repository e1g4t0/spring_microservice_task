package com.gym.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@FeignClient(value = "trainer-service",fallback = TrainerWorkoutClientFallback.class)
public interface TrainerWorkloadClient {

    @PostMapping("/trainer-workload")
    String addTraining(@RequestHeader("Authorization") String token,
                        @RequestParam String username,
                        @RequestParam String firstname,
                        @RequestParam String lastname,
                        @RequestParam Boolean isActive,
                        @RequestParam
                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date,
                        @RequestParam Double duration);
}
