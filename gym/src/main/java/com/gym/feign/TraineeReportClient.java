package com.gym.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "trainee-service",fallback = TrainerWorkoutClientFallback.class)
public interface TraineeReportClient {

    @GetMapping
    String getReport(String username);
}
