package com.gym.feign;

import org.springframework.stereotype.Component;

@Component
public class TraineeReportClientFallback implements TraineeReportClient{
    @Override
    public String getReport(String username) {
        return "fallback";
    }

}
