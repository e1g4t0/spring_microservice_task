package com.gym.mapper;

import com.gym.dto.TrainingDTO;
import com.gym.entity.TrainingEntity;
import org.springframework.stereotype.Component;
public class TrainingMapper {

    public static TrainingDTO getTrainingDTOFromTrainingEntity(TrainingEntity trainingEntity){

        TrainingDTO trainingDTO = new TrainingDTO();

        trainingDTO.setTrainingName(trainingEntity.getTrainingName());
        trainingDTO.setTrainingDate(trainingEntity.getTrainingDate());
        trainingDTO.setTrainingType(trainingEntity.getTrainingType());
        trainingDTO.setTrainingDuration(trainingEntity.getTrainingDuration());
        trainingDTO.setTraineeUsername(trainingEntity.getTrainee().getUser().getUsername());
        trainingDTO.setTrainerUsername(trainingEntity.getTrainer().getUser().getUsername());

        return trainingDTO;
    }
}
