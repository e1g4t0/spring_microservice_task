package com.gym.mapper;

import com.gym.dto.TraineeDTO;
import com.gym.dto.TraineeForTrainerDTO;
import com.gym.dto.TrainerForTraineeDTO;
import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
public class TraineeMapper {

    public static TraineeForTrainerDTO getTraineeForTrainerDTO(TraineeEntity trainee){

        TraineeForTrainerDTO traineeForTrainerDTO = new TraineeForTrainerDTO();
        UserEntity user = trainee.getUser();
        traineeForTrainerDTO.setUsername(user.getUsername());
        traineeForTrainerDTO.setFirstName(user.getFirstName());
        traineeForTrainerDTO.setLastName(user.getLastName());

        return traineeForTrainerDTO;
    }
    public static TraineeDTO getTraineeDTOFromTraineeEntity(TraineeEntity traineeEntity){

        TraineeDTO traineeDTO = new TraineeDTO();
        UserEntity user = traineeEntity.getUser();
        traineeDTO.setUsername(user.getUsername());
        traineeDTO.setFirstName(user.getFirstName());
        traineeDTO.setLastName(user.getLastName());
        traineeDTO.setActive(user.isActive());
        traineeDTO.setDateOfBirth(traineeEntity.getDateOfBirth());
        traineeDTO.setAddress(traineeEntity.getAddress());

        Set<TrainerForTraineeDTO> trainers = new HashSet<>();
        for(TrainingEntity training : traineeEntity.getTrainings()){
            trainers.add(TrainerMapper.getTrainerForTraineeDTOFromTrainerEntity(training.getTrainer()));
        }
        traineeDTO.setTrainers(trainers);

        return traineeDTO;
    }
}
