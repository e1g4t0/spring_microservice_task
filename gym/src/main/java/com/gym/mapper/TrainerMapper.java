package com.gym.mapper;

import com.gym.dto.TraineeForTrainerDTO;
import com.gym.dto.TrainerDTO;
import com.gym.dto.TrainerForTraineeDTO;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
public class TrainerMapper {
    public static TrainerForTraineeDTO getTrainerForTraineeDTOFromTrainerEntity(TrainerEntity trainer){

        TrainerForTraineeDTO trainerForTraineeDTO = new TrainerForTraineeDTO();
        UserEntity user = trainer.getUser();
        trainerForTraineeDTO.setUsername(user.getUsername());
        trainerForTraineeDTO.setFirstName(user.getFirstName());
        trainerForTraineeDTO.setLastName(user.getLastName());
        trainerForTraineeDTO.setSpecialization(trainer.getSpecialization());

        return trainerForTraineeDTO;
    }

    public static TrainerDTO getTrainerDTOFromTrainerEntity(TrainerEntity trainerEntity){

        TrainerDTO trainerDTO = new TrainerDTO();
        trainerDTO.setUsername(trainerEntity.getUser().getUsername());
        trainerDTO.setFirstName(trainerEntity.getUser().getFirstName());
        trainerDTO.setLastName(trainerEntity.getUser().getLastName());
        trainerDTO.setActive(trainerEntity.getUser().isActive());
        trainerDTO.setSpecialization(trainerEntity.getSpecialization());

        Set<TraineeForTrainerDTO> trainees = new HashSet<>();

        for (TrainingEntity training : trainerEntity.getTrainings()){
            trainees.add(TraineeMapper.getTraineeForTrainerDTO(training.getTrainee()));
        }

        trainerDTO.setTrainees(trainees);

        return trainerDTO;
    }
}
