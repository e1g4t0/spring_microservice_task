package com.gym.mapper;

import com.gym.dto.UserCredentialsDTO;
import com.gym.entity.UserEntity;
import org.springframework.stereotype.Component;
public class UserCredentialsMapper {

    public static UserCredentialsDTO getUserCredentialsDTOFromUser(UserEntity user){

        UserCredentialsDTO userCredentialsDTO = new UserCredentialsDTO();
        userCredentialsDTO.setUsername(user.getUsername());
        userCredentialsDTO.setPassword(user.getPassword());

        return userCredentialsDTO;
    }
}
