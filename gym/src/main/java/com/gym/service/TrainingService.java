package com.gym.service;

import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TrainingService {
    private final TrainingRepository trainingRepository;
    @Autowired
    public TrainingService(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    public TrainingEntity create(TraineeEntity trainee, TrainerEntity trainer, String name,
                                 TrainingTypeEntity trainingType, Date dateOfTraining, Double duration){

        TrainingEntity training = new TrainingEntity();

        training.setTrainee(trainee);
        training.setTrainer(trainer);
        training.setTrainingName(name);
        training.setTrainingType(trainingType);
        training.setTrainingDate(dateOfTraining);
        training.setTrainingDuration(duration);

        return trainingRepository.save(training);
    }


}
