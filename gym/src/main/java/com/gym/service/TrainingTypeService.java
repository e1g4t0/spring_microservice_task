package com.gym.service;

import com.gym.entity.TrainingTypeEntity;
import com.gym.repository.TrainingTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingTypeService {
    private final TrainingTypeRepository trainingTypeRepository;
    @Autowired
    public TrainingTypeService(TrainingTypeRepository trainingTypeRepository) {
        this.trainingTypeRepository = trainingTypeRepository;
    }

    public TrainingTypeEntity findById(Long id){
        return trainingTypeRepository.findById(id).get();
    }

    public List<TrainingTypeEntity> getAllTrainingTypes(){

        return trainingTypeRepository.findAll();
    }

    public TrainingTypeEntity getByName(String trainingTypeName){

        return trainingTypeRepository.findByTrainingTypeName(trainingTypeName);
    }
}
