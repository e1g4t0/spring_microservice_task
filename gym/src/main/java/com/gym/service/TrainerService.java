package com.gym.service;

import com.gym.entity.*;
import com.gym.manager.CredentialsManager;
import com.gym.repository.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TrainerService {
    private final TrainerRepository trainerRepository;
    private final CredentialsManager credentialsManager;

    private final BCryptPasswordEncoder passwordEncoder;
    @Autowired
    public TrainerService(TrainerRepository trainerRepository, CredentialsManager credentialsManager1, BCryptPasswordEncoder passwordEncoder) {
        this.trainerRepository = trainerRepository;
        this.credentialsManager = credentialsManager1;
        this.passwordEncoder = passwordEncoder;
    }

    public TrainerEntity create(String firstname, String lastname, String specialization){

        TrainerEntity trainer = new TrainerEntity();
        String password = credentialsManager.generatePassword();
        UserEntity user = new UserEntity(firstname, lastname, generateUsername(firstname, lastname), passwordEncoder.encode(password), true);
        trainer.setSpecialization(specialization);
        trainer.setUser(user);
        TrainerEntity createdTrainer = trainerRepository.save(trainer);
        createdTrainer.getUser().setPassword(password);

        return createdTrainer;
    }
    public TrainerEntity getTrainerByUserName(String username){
        return trainerRepository.findByUserUsername(username);
    }

    public void delete(String trainerUsername){
        TrainerEntity trainer = getTrainerByUserName(trainerUsername);
        trainerRepository.delete(trainer);
    }

    public TrainerEntity changeActivationStatus(String username){

        TrainerEntity trainer = trainerRepository.findByUserUsername(username);
        boolean isTrainerActive = trainer.getUser().isActive();
        trainer.getUser().setActive(!isTrainerActive);

        return trainerRepository.save(trainer);
    }

    public TrainerEntity updateTrainerProfile(String username, String firstname, String lastname, String specialization, Boolean isActive){

        TrainerEntity trainer = getTrainerByUserName(username);

        if(firstname != null){
            trainer.getUser().setFirstName(firstname);
        }

        if(lastname != null){
            trainer.getUser().setLastName(lastname);
        }

        if (specialization != null){
            trainer.setSpecialization(specialization);
        }

        if (isActive != null){
            trainer.getUser().setActive(isActive);
        }

        return trainerRepository.save(trainer);

    }

    public TrainerEntity changeLogin(String username, String newPassword){

        TrainerEntity trainerEntity = getTrainerByUserName(username);
        trainerEntity.getUser().setPassword(passwordEncoder.encode(newPassword));
        return trainerRepository.save(trainerEntity);
    }

    public List<TrainerEntity> getNotAssignedActiveTrainers(){
        return trainerRepository.findNotAssignedActiveTrainers();
    }
//    public List<TrainerEntity> getAllNotOnSpecificTraineeActive(TraineeEntity trainee){
//
//        return trainerRepository.getAllNotOnSpecificTraineeActive(trainee);
//    }

    public String generateUsername(String firstname, String lastname){

        StringBuilder username = new StringBuilder();
        username.append(firstname);
        username.append('.');
        username.append(lastname);
        TrainerEntity trainer = getTrainerByUserName(String.valueOf(username));

        long countUsername = 1;
        StringBuilder usernameDrop = new StringBuilder(username);

        while(trainer != null){
            countUsername++;
            usernameDrop = new StringBuilder(username);
            usernameDrop.append(countUsername);
            trainer = getTrainerByUserName(String.valueOf(usernameDrop));
        }

        return String.valueOf(usernameDrop);
    }

    public List<TrainingEntity> getAllTrainings(TrainerEntity trainer, TraineeEntity trainee, String trainingName,
                                                Date periodFrom, Date periodTo, TrainingTypeEntity trainingType){

        return trainerRepository.findAllTrainings(trainer, trainee, trainingName, periodFrom, periodTo, trainingType);
    }
}
