package com.gym.service;

import com.gym.entity.*;
import com.gym.manager.CredentialsManager;
import com.gym.repository.TraineeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TraineeService {

    private TraineeRepository traineeRepository;
    private CredentialsManager credentialsManager;
    private final BCryptPasswordEncoder passwordEncoder;
    @Autowired
    public TraineeService(TraineeRepository traineeRepository,
                          CredentialsManager credentialsManager, BCryptPasswordEncoder passwordEncoder) {
        this.traineeRepository = traineeRepository;
        this.credentialsManager = credentialsManager;
        this.passwordEncoder = passwordEncoder;
    }

    public TraineeEntity create(String firstname, String lastname, Date dateOfBirth, String address){

        TraineeEntity trainee = new TraineeEntity();
        String password = credentialsManager.generatePassword();
        UserEntity user = new UserEntity(firstname, lastname, generateUsername(firstname, lastname), passwordEncoder.encode(password), true);
        trainee.setDateOfBirth(dateOfBirth);
        trainee.setAddress(address);
        trainee.setUser(user);

        TraineeEntity createdTrainee = traineeRepository.save(trainee);
        createdTrainee.getUser().setPassword(password);
        return createdTrainee;
    }

    public TraineeEntity getTraineeByUserName(String username){
        return traineeRepository.findByUserUsername(username);
    }

    public TraineeEntity getById(Long id){
        return traineeRepository.findById(id).get();
    }

    public TraineeEntity updateTraineeProfile(String username, String firstname, String lastname,
                                              String address, Date dateOfBirth, Boolean isActive){

        TraineeEntity trainee = getTraineeByUserName(username);

        if(firstname != null){
            trainee.getUser().setFirstName(firstname);
        }

        if(lastname != null){
            trainee.getUser().setLastName(lastname);
        }

        if (address != null){
            trainee.setAddress(address);
        }

        if (dateOfBirth != null){
            trainee.setDateOfBirth(dateOfBirth);
        }

        if(isActive != null){
            trainee.getUser().setActive(isActive);
        }

        return traineeRepository.save(trainee);

    }

    public List<TrainingEntity> getAllTrainings(TraineeEntity trainee, TrainerEntity trainer, String trainingName, Date periodFrom, Date periodTo, TrainingTypeEntity trainingType){

        List<TrainingEntity> trainingEntities = traineeRepository.findAllTrainings(trainee, trainer, trainingName, periodFrom, periodTo, trainingType);
        return trainingEntities;
    }

    public void delete(String username){

        TraineeEntity trainee = getTraineeByUserName(username);
        traineeRepository.delete(trainee);
    }

    public TraineeEntity changeLogin(String username, String newPassword){

        TraineeEntity traineeEntity = getTraineeByUserName(username);
        traineeEntity.getUser().setPassword(passwordEncoder.encode(newPassword));
        return traineeRepository.save(traineeEntity);
    }

    public TraineeEntity changeActivationStatus(String username){

        TraineeEntity trainee = traineeRepository.findByUserUsername(username);
        boolean isTraineeActive = trainee.getUser().isActive();
        trainee.getUser().setActive(!isTraineeActive);

        return traineeRepository.save(trainee);
    }

    public String generateUsername(String firstname, String lastname){

        StringBuilder username = new StringBuilder();
        username.append(firstname);
        username.append('.');
        username.append(lastname);
        TraineeEntity trainee = getTraineeByUserName(String.valueOf(username));

        long countUsername = 1;
        StringBuilder usernameDrop = new StringBuilder(username);

        while(trainee != null){
            countUsername++;
            usernameDrop = new StringBuilder(username);
            usernameDrop.append(countUsername);
            trainee = getTraineeByUserName(String.valueOf(usernameDrop));
        } //

        return String.valueOf(usernameDrop);
    }

}
