package com.gym;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
@ComponentScan(basePackages = "com.gym")
@EnableDiscoveryClient
@EnableFeignClients
public class GymApplication implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		for (String profileName : environment.getActiveProfiles()) {
			System.out.println("Currently active profile - " + profileName);
		}
//		System.out.println(traineeRepository.findAll());
//		System.out.println(traineeRepository.findByUserUsername("John.Doe1"));
	}

	@Autowired
	private Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(GymApplication.class, args);



		//System.out.println(traineeRepository.findByUserUsername("John.Doe"));
	}

}
