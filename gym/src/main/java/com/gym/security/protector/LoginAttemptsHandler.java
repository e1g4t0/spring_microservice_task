package com.gym.security.protector;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
@Component
public class LoginAttemptsHandler implements HandlerInterceptor {

    private final ConcurrentHashMap<String, AtomicInteger> attemptsCache = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Date> blockList = new ConcurrentHashMap<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

        if (!request.getMethod().equals("GET")){
            return true;
        }

        String username = request.getParameter("username");

        if (!blockList.containsKey(username)){

            return true;
        }

        if (blockList.get(username).before(new Date())){

            blockList.remove(username);
            attemptsCache.remove(username);
            return true;
        }
        response.sendError(403, "The user has been added into blacklist");
        return false;
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                Exception ex) {

        if(response.getStatus() != 200){

            String username = request.getParameter("username");
            AtomicInteger attempt = attemptsCache.getOrDefault(username, new AtomicInteger(0));
            attempt.incrementAndGet();
            attemptsCache.put(username, attempt);

            if(attempt.get() >= 3){
                blockList.put(username, new Date(System.currentTimeMillis() + 300000));
            }
        }
    }

}
