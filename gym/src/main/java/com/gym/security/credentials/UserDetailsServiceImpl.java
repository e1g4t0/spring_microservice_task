package com.gym.security.credentials;

import com.gym.entity.UserEntity;
import com.gym.repository.TraineeRepository;
import com.gym.repository.TrainerRepository;
import com.gym.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TraineeRepository traineeRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity userEntity = userRepository.findByUsername(username);

        if (userEntity == null){
            throw new UsernameNotFoundException("Username not found");
        }

        UserDetailsImpl userDetails = new UserDetailsImpl(userEntity);
        if(traineeRepository.findByUserUsername(userEntity.getUsername()) != null){
            userDetails.setRole("TRAINEE");
        } else{
            userDetails.setRole("TRAINER");
        }

        return userDetails;
    }
}
