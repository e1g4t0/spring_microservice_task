package com.gym.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "training_type")
public class TrainingTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "training_type_id")
    private Long id;
    @NotNull
    @Column(name = "training_type_name")
    private String trainingTypeName;


    public TrainingTypeEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrainingTypeName() {
        return trainingTypeName;
    }

    public void setTrainingTypeName(String trainingTypeName) {
        this.trainingTypeName = trainingTypeName;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(!(obj instanceof TrainingTypeEntity)){
            return false;
        }

        return this.id.equals(((TrainingTypeEntity) obj).id);
    }
}
