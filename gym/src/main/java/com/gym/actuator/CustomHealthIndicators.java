package com.gym.actuator;
import org.json.JSONObject;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

@Component
public class CustomHealthIndicators extends AbstractHealthIndicator {

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {

        var client = HttpClient.newHttpClient();

        var request = HttpRequest.newBuilder(
                        URI.create("http://localhost:8080/remote-service"))
                .GET()
                .build();

        String serviceStatus = "UP";

        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() != 200){
                serviceStatus = "Bad response";
            }

        } catch (IOException | InterruptedException e) {

            serviceStatus = "DOWN";
            builder.down().withDetail("RemoteService", serviceStatus);
            return;
        }

        builder.up().withDetail("RemoteService", serviceStatus);


        var requestWeather = HttpRequest.newBuilder(
                        URI.create("http://api.weatherapi.com/v1/current.json?key=6564680e28c749b1a5714751231910&q=London"))
                .header("Accept", "application/json")
                .GET()
                .build();

        try {
            HttpResponse<String> response = client.send(requestWeather, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 200) {

                builder.up().withDetail("WeatherService", "Service is UP");

            } else {

                builder.up().withDetail("WeatherService", "Bad response " + response.statusCode());
            }
        } catch (IOException | InterruptedException e) {

            builder.down().withDetail("WeatherService", "Weather Service is Down");
        }

    }
}
