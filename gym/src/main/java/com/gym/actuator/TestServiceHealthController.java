package com.gym.actuator;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/remote-service")
public class TestServiceHealthController {

    @GetMapping
    ResponseEntity returnStatusCode(){

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
