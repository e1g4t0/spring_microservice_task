package com.gym.facade;

import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.service.TraineeService;
import com.gym.service.TrainerService;
import com.gym.service.TrainingService;
import com.gym.service.TrainingTypeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class GymFacade {
    private TraineeService traineeService;
    private TrainerService trainerService;
    private TrainingService trainingService;
    private TrainingTypeService trainingTypeService;

    public GymFacade(TraineeService traineeService, TrainerService trainerService, TrainingService trainingService, TrainingTypeService trainingTypeService) {
        this.traineeService = traineeService;
        this.trainerService = trainerService;
        this.trainingService = trainingService;
        this.trainingTypeService = trainingTypeService;
    }

//    public TraineeEntity createTraineeProfile(String firstname, String lastname, String dateOfBirth, String address){
//
//        logger.info(" Trainee has been created");
//
//        //System.out.println(traineeService.getTraineeByUserName("JohnDoe"));
//        return traineeService.create(firstname, lastname, dateOfBirth, address);
//    }
//
//    public TraineeEntity getTraineeByUsername(String username){
//
//        return traineeService.getTraineeByUserName(username);
//    }
//
//    public TraineeEntity updateTraineeProfile(String username, String password , String firstname, String lastname, String address){
//
//
//
//        if(!traineeService.authenticateTrainee(username, password)){
//            System.out.println("Unable to authenticate trainee");
//            return null;
//        }
//        logger.info(" Trainee has been updated");
//        return traineeService.updateTraineeProfile(username, firstname, lastname, address);
//    }
//    public TraineeEntity regenerateTraineePassword(String username, String oldPassword){
//
//        if(!traineeService.authenticateTrainee(username, oldPassword)){
//            System.out.println("Unable to authenticate trainee");
//            return null;
//        }
//        return traineeService.regeneratePassword(username);
//    }
//
//    public TraineeEntity changeTraineeActivationStatus(String username, String password){
//
//        if(!traineeService.authenticateTrainee(username, password)){
//            System.out.println("Unable to authenticate trainee");
//            return null;
//        }
//        logger.info(" Trainee Activation status has been changed");
//        return traineeService.changeActivationStatus(username);
//    }
//    public void deleteTrainee(String username, String password){
//        if(!traineeService.authenticateTrainee(username, password)){
//            System.out.println("Unable to authenticate trainee");
//            return;
//        }
//        logger.info(" Trainee has been deleted");
//        traineeService.delete(username);
//    }
//
//    public TrainerEntity createTrainerProfile(String firstname, String lastname, String specialization){
//
//        return trainerService.create(firstname, lastname, specialization);
//    }
//
//    public TrainerEntity getTrainerByUsername(String username){
//
//        return trainerService.getTrainerByUserName(username);
//    }
//
//    public TrainerEntity updateTrainerProfile(String username, String password , String firstname, String lastname, String specialization){
//
//        if(!trainerService.authenticateTrainer(username, password)){
//            System.out.println("Unable to authenticate trainer");
//            return null;
//        }
//
//        return trainerService.updateTrainerProfile(username, firstname, lastname, specialization);
//    }
//
//    public TrainerEntity regenerateTrainerPassword(String username, String oldPassword){
//
//        if(!trainerService.authenticateTrainer(username, oldPassword)){
//            System.out.println("Unable to authenticate trainer");
//            return null;
//        }
//
//        return trainerService.regeneratePassword(username);
//    }
//
//    public TrainerEntity changeTrainerActivationStatus(String username, String password){
//
//        if(!trainerService.authenticateTrainer(username, password)){
//            System.out.println("Unable to authenticate trainer");
//            return null;
//        }
//        logger.info(" Trainer Activation status has been changed");
//        return trainerService.changeActivationStatus(username);
//    }

    public TrainingEntity addTraining(String traineeUsername, String trainerUsername, String name,
                                      Long trainingTypeId, Date dateOfTraining, Double duration){

        TraineeEntity trainee = traineeService.getTraineeByUserName(traineeUsername);
        TrainerEntity trainer = trainerService.getTrainerByUserName(trainerUsername);
        TrainingTypeEntity trainingType = trainingTypeService.findById(trainingTypeId);
        return trainingService.create(trainee, trainer, name, trainingType, dateOfTraining, duration);
    }

    public List<TrainingEntity> getAllTraineeTrainings(String traineeUsername, String trainerUsername, String trainingName,
                                                       Date periodFrom, Date periodTo, TrainingTypeEntity trainingTypeEntity){

        TraineeEntity trainee = traineeService.getTraineeByUserName(traineeUsername);
        TrainerEntity trainer = trainerService.getTrainerByUserName(trainerUsername);
        return traineeService.getAllTrainings(trainee, trainer, trainingName,
                periodFrom, periodTo, trainingTypeEntity);
    }

    public List<TrainingEntity> getAllTrainerTrainings(String trainerUsername,String traineeUsername, String trainingName,
                                                      Date periodFrom, Date periodTo, TrainingTypeEntity trainingTypeEntity){

        TraineeEntity trainee = traineeService.getTraineeByUserName(traineeUsername);
        TrainerEntity trainer = trainerService.getTrainerByUserName(trainerUsername);

        return trainerService.getAllTrainings(trainer, trainee, trainingName,
                periodFrom, periodTo, trainingTypeEntity);
    }

}
