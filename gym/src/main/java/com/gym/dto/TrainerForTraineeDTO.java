package com.gym.dto;

public class TrainerForTraineeDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String specialization;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(!(obj instanceof TrainerForTraineeDTO)){

            return false;
        }

        return this.username.equals(((TrainerForTraineeDTO) obj).username);
    }
}
