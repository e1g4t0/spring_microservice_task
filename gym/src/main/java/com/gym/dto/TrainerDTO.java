package com.gym.dto;

import java.util.HashSet;
import java.util.Set;

public class TrainerDTO {
    private String username;
    private String firstName;
    private String lastName;
    private Boolean isActive;
    private String specialization;
    private Set<TraineeForTrainerDTO> trainees = new HashSet<>();

    public TrainerDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Set<TraineeForTrainerDTO> getTrainees() {
        return trainees;
    }

    public void setTrainees(Set<TraineeForTrainerDTO> trainees) {
        this.trainees = trainees;
    }
}
