package com.gym.dto;

import com.gym.entity.TrainingTypeEntity;

import java.util.Date;

public class TrainingDTO {

    private String trainingName;
    private Date trainingDate;
    private TrainingTypeEntity trainingType;
    private Double trainingDuration;
    private String traineeUsername;
    private String trainerUsername;

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }

    public Date getTrainingDate() {
        return trainingDate;
    }

    public void setTrainingDate(Date trainingDate) {
        this.trainingDate = trainingDate;
    }

    public TrainingTypeEntity getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(TrainingTypeEntity trainingType) {
        this.trainingType = trainingType;
    }

    public Double getTrainingDuration() {
        return trainingDuration;
    }

    public void setTrainingDuration(Double trainingDuration) {
        this.trainingDuration = trainingDuration;
    }

    public String getTraineeUsername() {
        return traineeUsername;
    }

    public void setTraineeUsername(String traineeUsername) {
        this.traineeUsername = traineeUsername;
    }

    public String getTrainerUsername() {
        return trainerUsername;
    }

    public void setTrainerUsername(String trainerUsername) {
        this.trainerUsername = trainerUsername;
    }
}
