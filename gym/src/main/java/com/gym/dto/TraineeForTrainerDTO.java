package com.gym.dto;

public class TraineeForTrainerDTO {
    private String username;
    private String firstName;
    private String lastName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(!(obj instanceof TraineeForTrainerDTO)){

            return false;
        }


        return this.username.equals(((TraineeForTrainerDTO) obj).username);
    }
}
