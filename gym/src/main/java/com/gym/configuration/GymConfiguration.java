package com.gym.configuration;
import com.gym.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(basePackages = {"com.gym"}/*{"com.gym.entity", "com.gym.facade", "com.gym.repository", "com.gym.service"}*/)
public class GymConfiguration {
    //private static final Logger logger = LogManager.getLogger(GymConfiguration.class);

//    @Bean
//    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
//    public SessionFactory buildSessionFactory(){
//        try {
//            // Create the SessionFactory from hibernate.cfg.xml
//
//            org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration().configure();
//            configuration.addAnnotatedClass(TraineeEntity.class);
//            configuration.addAnnotatedClass(UserEntity.class);
//            configuration.addAnnotatedClass(TrainerEntity.class);
//            configuration.addAnnotatedClass(TrainingTypeEntity.class);
//            configuration.addAnnotatedClass(TrainingEntity.class);
//            ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
//
//            SessionFactory sessionFactory = configuration.buildSessionFactory(registry);
//
//            logger.info("Session factory has been created");
//            return sessionFactory;
//        } catch (Throwable ex) {
//            System.err.println("Initial SessionFactory creation failed." + ex);
//            throw new ExceptionInInitializerError(ex);
//        }
//    }

}
