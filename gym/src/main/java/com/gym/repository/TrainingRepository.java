package com.gym.repository;

import com.gym.entity.TrainingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface TrainingRepository extends JpaRepository<TrainingEntity, Long> {

}
