package com.gym.repository;

import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TraineeRepository extends JpaRepository<TraineeEntity, Long> {

    TraineeEntity findByUserUsername(String username);

    @Query("SELECT t FROM TrainingEntity t WHERE t.trainee = :trainee AND " +
            "(:trainer is null OR t.trainer = :trainer) AND " +
            "(:trainingName is null OR t.trainingName = :trainingName) AND " +
            "(cast(:periodFrom as date) is null OR t.trainingDate >= :periodFrom) AND " +
            "(cast(:periodTo as date) is null OR t.trainingDate <= :periodTo) AND " +
            "(:trainingType is null OR t.trainingType = :trainingType)")
    List<TrainingEntity> findAllTrainings(@Param("trainee") TraineeEntity trainee, @Param("trainer") TrainerEntity trainer, @Param("trainingName") String trainingName,
                                          @Param("periodFrom") Date periodFrom, @Param("periodTo") Date periodTo, @Param("trainingType") TrainingTypeEntity trainingType);


}
