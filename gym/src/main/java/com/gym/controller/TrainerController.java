package com.gym.controller;

import com.gym.dto.TrainerDTO;
import com.gym.dto.TrainingDTO;
import com.gym.dto.UserCredentialsDTO;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.facade.GymFacade;
import com.gym.mapper.TrainerMapper;
import com.gym.mapper.TrainingMapper;
import com.gym.mapper.UserCredentialsMapper;
import com.gym.security.credentials.UserDetailsServiceImpl;
import com.gym.security.jwt.JwtService;
import com.gym.service.TrainerService;
import com.gym.service.TrainingTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.management.remote.JMXAuthenticator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/trainers")
@CrossOrigin
public class TrainerController {
    @Autowired
    private TrainerService trainerService;
    @Autowired
    private TrainingTypeService trainingTypeService;
    @Autowired
    private GymFacade gymFacade;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private JwtService jwtService;

    @PostMapping
    ResponseEntity<UserCredentialsDTO> create(@RequestParam String firstname,
                                              @RequestParam String lastname,
                                              @RequestParam String specialization) {

        TrainingTypeEntity trainingTypeEntity = trainingTypeService.getByName(specialization);

        if(trainingTypeEntity == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Status", "Training type does not exist").build();
        }

        TrainerEntity createdTrainer = trainerService.create(firstname, lastname, specialization);
        UserCredentialsDTO userCredentialsDTO = UserCredentialsMapper.getUserCredentialsDTOFromUser(createdTrainer.getUser());

        return new ResponseEntity<>(userCredentialsDTO, HttpStatus.CREATED);
    }

    @GetMapping(value = "/login")
    ResponseEntity login(@RequestParam String username,
                         @RequestParam String password){

        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User Expired");
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User Credentials are incorrect");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        final String token = jwtService.generateToken(userDetails, new HashMap<>());

        return ResponseEntity.status(HttpStatus.OK).body(token);
    }

    @PutMapping(value = "/login")
    ResponseEntity changeLogin(@RequestParam(required = false) String username,
                               @RequestParam(required = false) String oldPassword,
                               @RequestParam String newPassword){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(userDetails.getAuthorities());
        trainerService.changeLogin(userDetails.getUsername(), newPassword);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping
    ResponseEntity<TrainerDTO> getByUsername(@RequestParam(required = false) String username,
                                             @RequestParam(required = false) String password){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        TrainerEntity trainerEntity = trainerService.getTrainerByUserName(userDetails.getUsername());
        TrainerDTO trainer = TrainerMapper.getTrainerDTOFromTrainerEntity(trainerEntity);

        return new ResponseEntity<>(trainer, HttpStatus.OK);
    }
    @PutMapping
    ResponseEntity<TrainerDTO> update(@RequestParam(required = false) String username,
                                      @RequestParam(required = false) String password,
                                      @RequestParam String firstname,
                                      @RequestParam String lastname,
                                      @RequestParam String specialization,
                                      @RequestParam Boolean isActive){

        TrainingTypeEntity trainingTypeEntity = trainingTypeService.getByName(specialization);

        if(trainingTypeEntity == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Status", "Training type does not exist").build();
        }

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        TrainerEntity updatedTrainer = trainerService.updateTrainerProfile(userDetails.getUsername(), firstname, lastname, specialization, isActive);
        TrainerDTO trainer = TrainerMapper.getTrainerDTOFromTrainerEntity(updatedTrainer);

        return new ResponseEntity<>(trainer, HttpStatus.OK);
    }

    @GetMapping("/not-assigned-trainers")
    ResponseEntity<List<TrainerDTO>> getNotAssignedTriner(@RequestParam(required = false) String username,
                                                          @RequestParam(required = false) String password){

        List<TrainerEntity> trainerEntities = trainerService.getNotAssignedActiveTrainers();

        List<TrainerDTO> trainerDTOS = trainerEntities.stream().map(TrainerMapper::getTrainerDTOFromTrainerEntity).collect(Collectors.toList());

        return new ResponseEntity<>(trainerDTOS, HttpStatus.OK);
    }

    @PatchMapping
    ResponseEntity changeActivationStatus(@RequestParam(required = false) String username,
                                          @RequestParam(required = false) String password){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        trainerService.changeActivationStatus(userDetails.getUsername());

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/trainings")
    ResponseEntity<List<TrainingDTO>> getAllTrainings(@RequestParam(required = false) String username,
                                                      @RequestParam(required = false) String password,
                                                      @RequestParam(required = false)
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
                                                      @RequestParam(required = false)
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
                                                      @RequestParam(required = false) String trainingName,
                                                      @RequestParam(required = false) String traineeUsername,
                                                      @RequestParam(required = false) String trainingType){

        TrainingTypeEntity trainingTypeEntity = trainingTypeService.getByName(trainingType);

        if(trainingType != null && trainingTypeEntity == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Status", "Training type does not exist").build();
        }

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<TrainingEntity> trainingEntities = gymFacade.getAllTrainerTrainings(userDetails.getUsername(), traineeUsername, trainingName,
                periodFrom, periodTo, trainingTypeEntity);

        List<TrainingDTO> trainingDTOS = trainingEntities.stream().map(TrainingMapper::getTrainingDTOFromTrainingEntity).toList();

        return new ResponseEntity<>(trainingDTOS, HttpStatus.OK);
    }

}
