package com.gym.controller;

import com.gym.dto.TrainingDTO;
import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainerEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.feign.TrainerWorkloadClient;
import com.gym.mapper.TrainingMapper;
import com.gym.service.TraineeService;
import com.gym.service.TrainerService;
import com.gym.service.TrainingService;
import com.gym.service.TrainingTypeService;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping(value = "/trainings")
@EnableFeignClients
@CrossOrigin
public class TrainingController {

    @Autowired
    private TrainingService trainingService;
    @Autowired
    private TrainingTypeService trainingTypeService;
    @Autowired
    private TrainerService trainerService;
    @Autowired
    private TraineeService traineeService;
    @Autowired
    private TrainerWorkloadClient trainerWorkloadClient;

    @PostMapping
    ResponseEntity<Object> create(@RequestParam String traineeUsername,
                                          @RequestParam String trainerUsername,
                                          @RequestParam String trainingName,
                                          @RequestParam String trainingType,
                                          @RequestParam
                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateOfTraining,
                                          @RequestParam Double duration){
        TrainingTypeEntity trainingTypeEntity = trainingTypeService.getByName(trainingType);

        if(trainingTypeEntity == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Status", "Training type does not exist").build();
        }

        TraineeEntity trainee = traineeService.getTraineeByUserName(traineeUsername);
        TrainerEntity trainer = trainerService.getTrainerByUserName(trainerUsername);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String response = trainerWorkloadClient.addTraining("Bearer " + authentication.getCredentials(), trainerUsername,trainer.getUser().getFirstName(), trainer.getUser().getLastName(),
                trainer.getUser().isActive(), dateOfTraining, duration);

        if("fallback".equals(response)){
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Unfortunately, the trainer service is down");
        }

        TrainingEntity training = trainingService.create(trainee, trainer, trainingName, trainingTypeEntity, dateOfTraining, duration);

        if (training == null){
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        TrainingDTO trainingDTO = TrainingMapper.getTrainingDTOFromTrainingEntity(training);
        System.out.println(training.getTrainee());



        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
