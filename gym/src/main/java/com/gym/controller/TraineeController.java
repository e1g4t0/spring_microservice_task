package com.gym.controller;

import com.gym.dto.TraineeDTO;
import com.gym.dto.TrainingDTO;
import com.gym.dto.UserCredentialsDTO;
import com.gym.entity.TraineeEntity;
import com.gym.entity.TrainingEntity;
import com.gym.entity.TrainingTypeEntity;
import com.gym.facade.GymFacade;
import com.gym.feign.TraineeReportClient;
import com.gym.mapper.TraineeMapper;
import com.gym.mapper.TrainingMapper;
import com.gym.mapper.UserCredentialsMapper;
import com.gym.security.credentials.UserDetailsImpl;
import com.gym.security.credentials.UserDetailsServiceImpl;
import com.gym.security.jwt.JwtService;
import com.gym.security.protector.LoginAttemptsHandler;
import com.gym.service.TraineeService;
import com.gym.service.TrainingTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/trainees")
@Api(produces = "application/json", value = "Operations for creating, updating, retrieving and deleting Trainees in the application")
@CrossOrigin
public class TraineeController {

    @Autowired
    private GymFacade gymFacade;
    @Autowired
    private TrainingTypeService trainingTypeService;
    @Autowired
    private TraineeService traineeService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private LoginAttemptsHandler loginAttemptsHandler;
//    @Autowired
//    TraineeReportClient traineeReportClient;

    @PostMapping
    @ApiOperation(value = "Create a piece of Trainee", response = UserCredentialsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created Trainee"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    ResponseEntity<UserCredentialsDTO> create(@RequestParam String firstname,
                                              @RequestParam String lastname,
                                              @RequestParam(required = false)
                                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateOfBirth,
                                              @RequestParam(required = false) String address) {


        TraineeEntity createdTrainee = traineeService.create(firstname, lastname, dateOfBirth, address);
        UserCredentialsDTO userCredentialsDTO = UserCredentialsMapper.getUserCredentialsDTOFromUser(createdTrainee.getUser());

        return new ResponseEntity<>(userCredentialsDTO, HttpStatus.CREATED);
    }

    @GetMapping(value = "/login")
    @ApiOperation(value = "Authenticates Trainee")
    @ApiResponses(value = {
            @ApiResponse(code = 20, message = "Successfully authenticated Trainee"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated")
    }
    )
    ResponseEntity login(@RequestParam String username,
                         @RequestParam String password) throws Exception {

        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

        } catch (DisabledException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User Expired");
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User Credentials are incorrect");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        final String token = jwtService.generateToken(userDetails, new HashMap<>());

        return ResponseEntity.status(HttpStatus.OK).body(token);
    }

    @PutMapping(value = "/login")
    @ApiOperation(value = "Change Trainee login")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Login Successfully has been changed"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated")
    }
    )
    ResponseEntity changeLogin(@RequestParam(required = false) String username,
                               @RequestParam(required = false) String oldPassword,
                               @RequestParam String newPassword){


        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        traineeService.changeLogin(userDetails.getUsername(), newPassword);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Retrieves Trainee by username", response = TraineeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Trainee"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    ResponseEntity<TraineeDTO> getByUsername(@RequestParam(required = false) String username,
                                             @RequestParam(required = false) String password){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        TraineeEntity traineeEntity = traineeService.getTraineeByUserName(userDetails.getUsername());
        TraineeDTO traineeDTO = TraineeMapper.getTraineeDTOFromTraineeEntity(traineeEntity);

        return new ResponseEntity<>(traineeDTO, HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation(value = "Updates Trainee by username", response = TraineeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated Trainee"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    ResponseEntity<TraineeDTO> update(@RequestParam(required = false) String username,
                                      @RequestParam(required = false) String password,
                                      @RequestParam String firstname,
                                      @RequestParam String lastname,
                                      @RequestParam(required = false)
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateOfBirth,
                                      @RequestParam(required = false) String address,
                                      @RequestParam Boolean isActive){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();


        TraineeEntity updatedTrainee = traineeService.updateTraineeProfile(userDetails.getUsername(), firstname, lastname, address, dateOfBirth, isActive);
        TraineeDTO traineeDTO = TraineeMapper.getTraineeDTOFromTraineeEntity(updatedTrainee);

        return new ResponseEntity<>(traineeDTO, HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation(value = "Deletes Trainee by username", response = TraineeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted Trainee"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated"),
    }
    )
    ResponseEntity delete(@RequestParam(required = false) String username,
                          @RequestParam(required = false) String password){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        //String response = traineeReportClient.getReport(userDetails.getUsername());

//        if("fallback".equals(response)){
//            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Unfortunately, the trainee service is down");
//        } else {
//            System.out.println(response);
//        }

        traineeService.delete(userDetails.getUsername());

        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping
    @ApiOperation(value = "Changes Trainee activation status by username", response = TraineeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully changed Trainee activation status"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    }
    )
    ResponseEntity changeActivationStatus(@RequestParam(required = false) String username,
                                          @RequestParam(required = false) String password){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();


        traineeService.changeActivationStatus(userDetails.getUsername());

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/trainings")
    @ApiOperation(value = "Retrieves all Trainee's trainings by given parameters", response = TraineeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved trainee's trainings"),
            @ApiResponse(code = 401, message = "The Trainee cannot be authenticated"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    }
    )
    ResponseEntity<List<TrainingDTO>> getAllTrainings(@RequestParam(required = false) String username,
                                                      @RequestParam(required = false) String password,
                                                      @RequestParam(required = false)
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
                                                      @RequestParam(required = false)
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
                                                      @RequestParam(required = false) String trainingName,
                                                      @RequestParam(required = false) String trainerUsername,
                                                      @RequestParam(required = false) String trainingType){


        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();


        TrainingTypeEntity trainingTypeEntity = trainingTypeService.getByName(trainingType);

        if(trainingType != null && trainingTypeEntity == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Status", "Training type does not exist").build();
        }

        List<TrainingEntity> trainingEntities = gymFacade.getAllTraineeTrainings(userDetails.getUsername(), trainerUsername, trainingName,
                periodFrom, periodTo, trainingTypeEntity);

        List<TrainingDTO> trainingDTOS = trainingEntities.stream().map(trainingEntity -> TrainingMapper.getTrainingDTOFromTrainingEntity(trainingEntity)).toList();

        return new ResponseEntity<>(trainingDTOS, HttpStatus.OK);
    }


}
