package com.trainer.controller;

import com.trainer.dto.TrainerWorkloadDto;
import com.trainer.feign.TrainerClient;
import com.trainer.service.TrainerWorkloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = "/trainer-workload")
@EnableFeignClients
public class TrainerWorkloadController {
    @Autowired
    TrainerWorkloadService trainerWorkloadService;
    @Autowired
    TrainerClient trainerClient;
    @PostMapping
    ResponseEntity create(@RequestParam String username,
                          @RequestParam String firstname,
                          @RequestParam String lastname,
                          @RequestParam Boolean isActive,
                          @RequestParam
                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date,
                          @RequestParam Double duration) throws InterruptedException {

        trainerWorkloadService.create(username, firstname, lastname, isActive, date, duration);

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping
    ResponseEntity delete(@RequestParam String username,
                          @RequestParam String firstname,
                          @RequestParam String lastname,
                          @RequestParam Boolean isActive,
                          @RequestParam
                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date,
                          @RequestParam Double duration){

        trainerWorkloadService.delete(username, firstname, lastname, isActive, date, duration);

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping
    ResponseEntity<Object> getWithSummary(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String json = trainerClient.getAllTrainersTrainings("Bearer " + authentication.getCredentials(),
                authentication.getName(), null, null, null, null, null, null);

        if (json.length() == 2){
            return new ResponseEntity<>(new TrainerWorkloadDto(), HttpStatus.OK);
        }

        if("fallback".equals(json)){
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Unfortunately, the main gym service is down");
        }

        TrainerWorkloadDto trainerWorkloadDto = trainerWorkloadService.getTrainerSummary(json);
        return new ResponseEntity<>(trainerWorkloadDto, HttpStatus.OK);
    }
}
