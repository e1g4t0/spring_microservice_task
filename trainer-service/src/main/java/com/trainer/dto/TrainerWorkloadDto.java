package com.trainer.dto;
import java.util.Map;

public class TrainerWorkloadDto {
    private String username;
    private Boolean isActive;
    private Map<Integer, Map<Integer, Double>> summaryDuration;
    public TrainerWorkloadDto() {
    }

    public String getUsername() {
        return username;
    }

    public TrainerWorkloadDto(String username, Boolean isActive, Map<Integer, Map<Integer, Double>> summaryDuration) {
        this.username = username;

        this.isActive = isActive;
        this.summaryDuration = summaryDuration;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Map<Integer, Map<Integer, Double>> getSummaryDuration() {
        return summaryDuration;
    }

    public void setSummaryDuration(Map<Integer, Map<Integer, Double>> summaryDuration) {
        this.summaryDuration = summaryDuration;
    }
}
