package com.trainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
@ComponentScan(basePackages = "com.trainer")
@EnableDiscoveryClient
@EnableFeignClients
public class TrainerService implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {

		for (String profileName : environment.getActiveProfiles()) {
			System.out.println("Currently active profile - " + profileName);
		}

	}

	@Autowired
	private Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(TrainerService.class, args);

	}

}
