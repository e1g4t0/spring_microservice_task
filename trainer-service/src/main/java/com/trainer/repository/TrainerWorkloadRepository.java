package com.trainer.repository;

import com.trainer.entity.TrainerWorkloadEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
@Transactional
public interface TrainerWorkloadRepository extends JpaRepository<TrainerWorkloadEntity, Long>{
    @Modifying
    @Query("DELETE FROM TrainerWorkloadEntity t WHERE t.username = :username AND " +
            "(:firstName is null OR t.firstName = :firstName) AND " +
            "(:lastName is null OR t.lastName = :lastName) AND " +
            "(:isActive is null OR t.isActive = :isActive) AND " +
            "(cast(:trainingDate as date) is null OR t.trainingDate = :trainingDate) AND " +
            "(:trainingDuration is null OR t.trainingDuration = :trainingDuration)")
    void deleteTrainerWorkload(String username, String firstName, String lastName,
                               Boolean isActive, Date trainingDate, Double trainingDuration);
}
