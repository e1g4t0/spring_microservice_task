package com.trainer.service;

import com.trainer.dto.TrainerWorkloadDto;
import com.trainer.entity.TrainerWorkloadEntity;
import com.trainer.repository.TrainerWorkloadRepository;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

@Service
public class TrainerWorkloadService {

    @Autowired
    TrainerWorkloadRepository trainerWorkloadRepository;

    public void delete(String username, String firstname, String lastname,
                       Boolean isActive, Date date, Double duration){


        trainerWorkloadRepository.deleteTrainerWorkload(username, firstname, lastname, isActive, date, duration);
    }

    public TrainerWorkloadEntity create(String username, String firstname, String lastname,
                       Boolean isActive, Date date, Double duration){

        TrainerWorkloadEntity trainerWorkloadEntity = new TrainerWorkloadEntity(username, firstname, lastname, isActive, date, duration);

        return trainerWorkloadRepository.save(trainerWorkloadEntity);
    }

    public TrainerWorkloadDto getTrainerSummary(String jsonString){

        JSONParser jsonParser = new JSONParser();

        Map<Integer, Map<Integer, Double>> summary = new TreeMap<>();
        String username = null;
        try {

            Object object = jsonParser.parse(jsonString);
            JSONArray trainingsAsJson = (JSONArray) object;
            JSONObject objectForUsername = (JSONObject)trainingsAsJson.get(0);
            username = (String)objectForUsername.get("trainerUsername");
            for (Object o : trainingsAsJson) {

                JSONObject trainingObject = (JSONObject) o;
                String trainingDate = (String) trainingObject.get("trainingDate");
                Double trainingDuration = (Double) trainingObject.get("trainingDuration");

                if(trainingDate == null || trainingDuration == null){
                     continue;
                }
                String yearAsString = trainingDate.substring(0, 4);
                String monthAsString = trainingDate.substring(5, 7);

                int year = Integer.parseInt(yearAsString);
                int month = Integer.parseInt(monthAsString);

                Map<Integer, Double> currentYearMap = summary.getOrDefault(year, getDefaultMap());

                currentYearMap.put(month, currentYearMap.getOrDefault(month, 0.0) + trainingDuration);
                summary.put(year, currentYearMap);
            }



        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        TrainerWorkloadDto trainerWorkloadDto = new TrainerWorkloadDto(username, true, summary);
        trainerWorkloadDto.setSummaryDuration(summary);

        return trainerWorkloadDto;
    }

    private Map<Integer, Double> getDefaultMap(){
        Map<Integer, Double> map = new TreeMap<>();
        map.put(1, 0.0);
        map.put(2, 0.0);
        map.put(3, 0.0);
        map.put(4, 0.0);
        map.put(5, 0.0);
        map.put(6, 0.0);
        map.put(7, 0.0);
        map.put(8, 0.0);
        map.put(9, 0.0);
        map.put(10, 0.0);
        map.put(11, 0.0);
        map.put(12, 0.0);

        return map;
    }

}
