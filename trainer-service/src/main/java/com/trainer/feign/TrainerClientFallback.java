package com.trainer.feign;

import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class TrainerClientFallback implements TrainerClient{
    @Override
    public String getAllTrainersTrainings(String token,String username, String password, Date periodFrom, Date periodTo, String trainingName, String traineeUsername, String trainingType) {
        return "fallback";
    }
}
