package com.trainer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@FeignClient(value = "gym-service", fallback = TrainerClientFallback.class)
public interface TrainerClient {
    @RequestMapping("/trainers/trainings")
    String getAllTrainersTrainings(@RequestHeader("Authorization") String token,
                                   @RequestParam(required = false) String username,
                                   @RequestParam(required = false) String password,
                                   @RequestParam(required = false)
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
                                   @RequestParam(required = false)
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
                                   @RequestParam(required = false) String trainingName,
                                   @RequestParam(required = false) String traineeUsername,
                                   @RequestParam(required = false) String trainingType);
}
