package com.trainer;

;

import com.trainer.configuration.TrainerConfiguration;
import com.trainer.entity.TrainerWorkloadEntity;
import com.trainer.service.TrainerWorkloadService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
@SpringJUnitConfig(TrainerConfiguration.class)
@Transactional
public class TestTrainerService {

    @Autowired
    TrainerWorkloadService trainerWorkloadService;

    @Test
    public void createTest() {

        TrainerWorkloadEntity trainerWorkloadEntity = trainerWorkloadService.create("Trainer.Username", "Trainer", "Username", true,
                new Date(), 2.0);
        assertNotNull(trainerWorkloadEntity);

    }



}