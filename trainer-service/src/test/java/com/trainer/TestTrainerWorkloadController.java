package com.trainer;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestTrainerWorkloadController {
    @Autowired
    private MockMvc mockMvc;

    private final RestTemplate restTemplate = new RestTemplate();
    @Test
    public void getSummaryTrainingNoTrainings() throws Exception {

        String content = String.valueOf(restTemplate.postForEntity("http://localhost:8801/trainers?firstname=Ricardo&lastname=Milos&specialization=Jogging", null, String.class));
        Assertions.assertNotNull(content);

        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.indexOf("password") + 11, content.indexOf("password") + 21);

        Assertions.assertNotNull(username);
        Assertions.assertNotNull(password);

        String token = String.valueOf(restTemplate.getForEntity("http://localhost:8801/trainers/login?username=" + username + "&password=" + password, String.class));

        token = token.substring(token.indexOf(",") + 1, token.indexOf("Vary") - 2);
        Assertions.assertNotNull(token);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/trainer-workload").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();


    }

    @Test
    public void getSummaryTrainingWithTrainings() throws Exception {

        String content = String.valueOf(restTemplate.postForEntity("http://localhost:8801/trainers?firstname=Ricardo&lastname=Milos&specialization=Jogging", null, String.class));
        String trainee = String.valueOf(restTemplate.postForEntity("http://localhost:8801/trainees?firstname=trainee&lastname=surname&dateOfBirth=2000-12-11&address=traineeaddress", null, String.class));

        Assertions.assertNotNull(content);

        String username = content.substring(content.indexOf(":") + 2, content.indexOf("password") - 3);
        String password = content.substring(content.indexOf("password") + 11, content.indexOf("password") + 21);

        String usernameTrainee = trainee.substring(trainee.indexOf(":") + 2, trainee.indexOf("password") - 3);

        Assertions.assertNotEquals(username.length(), 0);
        Assertions.assertNotEquals(usernameTrainee.length(), 0);
        Assertions.assertNotEquals(password.length(), 0);

        String token = String.valueOf(restTemplate.getForEntity("http://localhost:8801/trainers/login?username=" + username + "&password=" + password, String.class));

        token = token.substring(token.indexOf(",") + 1, token.indexOf("Vary") - 2);

        Assertions.assertNotNull(token);

        HttpHeaders headerAuthorization = new HttpHeaders();
        headerAuthorization.set("Authorization", "Bearer " + token);

        HttpEntity<String> requestEntity = new HttpEntity<>(null, headerAuthorization);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://localhost:8801/trainings?traineeUsername=" + usernameTrainee + "&trainerUsername=" + username + "&trainingName=Swimming Session 222&dateOfTraining=2011-01-16&duration=5.0&trainingType=Swimming",
                HttpMethod.POST,
                requestEntity,
                String.class
        );

        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/trainer-workload").header("Authorization", "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        Assertions.assertNotNull(result.getResponse());

    }

//    @Test
//    public void createTrainerWorkload() throws Exception {
//
//        TrainerWorkloadEntity trainerWorkloadEntity = new TrainerWorkloadEntity("drop.trainer", "drop", "trainer",
//                true, new Date(), 2.0);
//
//        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/trainer-workload?username=drop.user&firstname=drop&lastname=user&isActive=true&duration=16.0&date=2004-01-17"))
//                .andExpect(MockMvcResultMatchers.status().isCreated())
//                .andReturn();
//
//        String json = result.ge
//
//
//
//    }
}
