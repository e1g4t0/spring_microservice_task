package com.trainee;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/trainee-report")
@EnableFeignClients
public class TraineeReportController {

    @GetMapping
    ResponseEntity<Object> getReport(@RequestParam String username){
        return ResponseEntity.status(HttpStatus.OK).body("Trainee " + username + " has been deleted");
    }
}
